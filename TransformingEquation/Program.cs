﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using TransformingEquation.Models;

namespace TransformingEquation
{
    class Program
    {
        private static string help = @"
If you want to run with file, please run ""TransformingEquatin -file path_to_filename"".
If you want to run with interactive mode, please run ""TransformingEquatin -interactive"".
        ";

        static void Main(string[] args)
        {
            if (args.Length == 2 && args[0] == "-file")
            {
                if (File.Exists(args[1]))
                {
                    RunWithFile(args[1]);
                    Console.Write("Please check the transforming results in {0}.out", args[1]);
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("File not exist: {0}", args[1]);
                    Console.Write("Please hit enter to exit: ");
                    Console.ReadLine();
                }
            }
            else if (args.Length == 1 && args[0] == "-interactive")
            {
                RunWithInteractiveMode();
            }
            else
            {
                Console.WriteLine(help);
                Console.Write("Please hit enter to exit: ");
                Console.ReadLine();
            }
        }

        private static void RunWithFile(string file_name)
        {
            string line;
            string out_file_name = String.Format("{0}.out", file_name);

            using (StreamReader file = new System.IO.StreamReader(file_name))
            {
                using (StreamWriter out_file = new System.IO.StreamWriter(out_file_name))
                {
                    // Read the file and display it line by line.
                    while ((line = file.ReadLine()) != null)
                    {
                        Console.WriteLine(line);
                        try
                        {
                            Equation eq = new Equation(line);
                            Console.WriteLine("==> {0}", eq.ToString());
                            out_file.WriteLine(line + " ==> " + eq.ToString());
                        }
                        catch (Exception ex)
                        {
                            if (ex is TermException || ex is EquationException)
                            {
                                Console.WriteLine("==> {0}", ex.Message);
                                out_file.WriteLine(line + " ==> " + ex.Message);
                            }
                            else
                            {
                                throw;
                            }
                        }
                    }
                }
            }
        }

        private static void RunWithInteractiveMode()
        {
            string line;
            string out_file_name = @"files\TransformingEquationTestFile.out";

            Console.WriteLine("Please input a valid equation.");
            Console.WriteLine("And check the result in files\TransformingEquationTestFile.out");

            using (StreamWriter out_file = new System.IO.StreamWriter(out_file_name))
            {
                while ((line = Console.ReadLine()) != null)
                {
                    if (line == "")
                    {
                        Console.WriteLine("Please input a valid equation.");
                    }
                    else
                    {
                        try
                        {
                            Equation eq = new Equation(line);
                            Console.WriteLine("==> {0}", eq.ToString());
                            out_file.WriteLine(line + " ==> " + eq.ToString());
                        }
                        catch (Exception ex)
                        {
                            if (ex is TermException || ex is EquationException)
                            {
                                Console.WriteLine("==> {0}", ex.Message);
                                out_file.WriteLine(line + " ==> " + ex.Message);
                            }
                            else
                            {
                                throw;
                            }
                        }
                    }
                }
            }
        }
    }
}
