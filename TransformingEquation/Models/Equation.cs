﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

namespace TransformingEquation.Models
{
    public class EquationException : Exception
    {
        public EquationException(string error_msg)
            : base(error_msg)
        {
        }
    }

    public class Equation
    {
        private SortedDictionary<string, Term> terms = new SortedDictionary<string, Term>(new TermKeyComparer());
        private string[] words;
        private Stack<bool> parentheses_signs;
        private bool current_parentheses_signs;

        public readonly string Input_equation_line;

        public Equation(string line)
        {
            Input_equation_line = line;
            SplitEquation(line);
            BuildTerms();
        }

        public override string ToString()
        {
            if (terms.Count == 0)
            {
                return "0 = 0";
            }
            string canonical_form = "";
            int count = 0;
            foreach (KeyValuePair<string, Term> kvp in terms)
            {
                count++;
                if (count < terms.Count)
                {
                    canonical_form += kvp.Value.ToString() + " + ";
                }
                else
                {
                    canonical_form += kvp.Value.ToString() + " = 0";
                }
            }
            return canonical_form.Replace("+ -", "- ");
        }

        // Build terms of equations
        private void BuildTerms()
        {
            string previous_word = String.Empty;
            bool is_left_side = true;

            foreach (string word in words)
            {
                if (word == "=")
                {
                    is_left_side = false;
                }
                else if (word != "+" && word != "-" && word != "0")
                {
                    Term term = new Term(word);
                    if (previous_word == "-")
                    {
                        term.MultiplyCoefficient(-1);
                    }
                    if (terms.ContainsKey(term.Key))
                    {
                        if (is_left_side)
                        {
                            terms[term.Key].AddCoefficient(term.Coefficient);
                        }
                        else
                        {
                            terms[term.Key].SubtractCoefficient(term.Coefficient);
                        }
                        if (terms[term.Key].Coefficient == 0.0F)
                        {
                            terms.Remove(term.Key);
                        }
                    }
                    else
                    {
                        if (!is_left_side)
                        {
                            term.MultiplyCoefficient(-1);
                        }
                        terms.Add(term.Key, term);
                    }
                }
                previous_word = word;
            }
        }

        // Split equation and strip parentheses
        private void SplitEquation(string line)
        {
            parentheses_signs = new Stack<bool>();
            current_parentheses_signs = true;

            words = line.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries);
            if (words[words.Length - 1] == "0" && words[words.Length - 2] == "=")
            {
                words = words.Take(words.Length - 2).ToArray();
            }
            for (int i = 0; i < words.Length; i++)
            {
                if (i % 2 == 1 && words[i] != "+" && words[i] != "-" && words[i] != "=")
                {
                    throw new EquationException("Invalid equation");
                }
                if (words[i].FirstOrDefault() == '(')
                {
                    words[i] = words[i].Substring(1);
                    if (i > 0 && words[i - 1] == "-")
                    {
                        parentheses_signs.Push(current_parentheses_signs);
                        current_parentheses_signs = false;
                    }
                    else
                    {
                        parentheses_signs.Push(current_parentheses_signs);
                        current_parentheses_signs = true;
                    }
                    handle_left_parentheses(i);
                }
                else if (words[i].LastOrDefault() == ')')
                {
                    words[i] = words[i].Substring(0, words[i].Length - 1);
                    current_parentheses_signs = parentheses_signs.Pop();
                    handle_right_parentheses(i);
                }
                else if (words[i] == "+")
                {
                    if (!current_parentheses_signs)
                    {
                        words[i] = "-";
                    }
                }
                else if (words[i] == "-")
                {
                    if (!current_parentheses_signs)
                    {
                        words[i] = "+";
                    }
                }
            }
        }

        private void handle_left_parentheses(int i)
        {
            if (words[i].FirstOrDefault() == '(')
            {
                words[i] = words[i].Substring(1);
                if (i > 0 && words[i - 1] == "-")
                {
                    parentheses_signs.Push(current_parentheses_signs);
                    current_parentheses_signs = false;
                }
                else
                {
                    parentheses_signs.Push(current_parentheses_signs);
                    current_parentheses_signs = true;
                }
                handle_left_parentheses(i);
            }
        }

        private void handle_right_parentheses(int i)
        {
            if (words[i].LastOrDefault() == ')')
            {
                words[i] = words[i].Substring(0, words[i].Length - 1);
                current_parentheses_signs = parentheses_signs.Pop();
            }
        }

    }
}
