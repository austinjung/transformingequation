﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace TransformingEquation.Models
{
    public class TermException : Exception
    {
        public TermException(string error_msg)
            : base(error_msg)
        {
        }
    }

    public class TermKeyComparer : IComparer<string>
    {
        // Sort by variables asc, and degrees des, ex) x^2y^4, x^3 , x^2y3, xy
        public int Compare(string left, string right)
        {
            if (left == String.Empty && right == String.Empty)
            {
                return 0;
            }
            char[] left_chars = left.ToCharArray();
            char[] right_chars = right.ToCharArray();
            int r_index, l_index;
            for (r_index = 0, l_index = 0; l_index < left_chars.Length && r_index < right_chars.Length; r_index++, l_index++)
            {
                if (right_chars[r_index] != left_chars[l_index])
                {
                    if (Char.IsNumber(right_chars[r_index]) && !Char.IsNumber(left_chars[l_index]))
                    {
                        return 1;
                    }
                    else if (Char.IsNumber(left_chars[l_index]) && !Char.IsNumber(right_chars[r_index]))
                    {
                        return -1;
                    }
                    else if (Char.IsNumber(left_chars[l_index]) && Char.IsNumber(right_chars[r_index]))
                    {
                        int left_number = 0;
                        while (l_index < left_chars.Length && Char.IsNumber(left_chars[l_index]))
                        {
                            left_number = left_number * 10 + (int)(left_chars[l_index++] - '0');
                        }
                        l_index--;
                        int right_number = 0;
                        while (r_index < right_chars.Length && Char.IsNumber(right_chars[r_index]))
                        {
                            right_number = right_number * 10 + (int)(right_chars[r_index++] - '0');
                        }
                        r_index--;
                        return right_number - left_number;
                    }
                    else
                    {
                        return left_chars[l_index] - right_chars[r_index];
                    }
                }
            }
            if (right_chars.Length == left_chars.Length)
            {
                return 0;
            }
            else if (right_chars.Length > left_chars.Length)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }
    }

    public class Term
    {
        public const string InvalidTermNotationMessage = @"Invalid Term Notation";

        public readonly string TermString;
        private string _key;
        public string Key 
        { 
            get { return _key; } 
            private set{ _key = value; }
        }
        private float _coefficient;
        public float Coefficient
        {
            get { return _coefficient; }
            private set { _coefficient = value; }
        }
        public readonly SortedDictionary<char, int> variables = new SortedDictionary<char, int>();

        public Term(string term_string)
        {
            TermString = term_string.Trim();
            ParseTermString();
        }

        public void MultiplyCoefficient(float num)
        {
            Coefficient *= num;
        }

        public void AddCoefficient(float num)
        {
            Coefficient += num;
        }

        public void SubtractCoefficient(float num)
        {
            Coefficient -= num;
        }

        public override string ToString()
        {
            if (Coefficient == 0.0)
            {
                if (variables.Count == 0)
                {
                    return "0";
                }
                else
                {
                    return String.Empty;
                }
            }
            else if (Key == "")
            {
                return String.Format("{0}", Coefficient);
            }
            else if (Coefficient == 1.0)
            {
                return String.Format("{0}", Key);
            }
            else if (Coefficient == -1.0)
            {
                return String.Format("-{0}", Key);
            }
            else
            {
                return String.Format("{0}{1}", Coefficient, Key);
            }
        }

        private void ParseTermString()
        {
            bool sign_acceptable = true;
            bool variable_acceptable = true;
            bool number_acceptable = true;
            bool degree_sign_acceptable = false;

            bool parsing_coefficient = true;
            bool parsing_degree_of_variable = false;

            char previous_variable = '\0';
            string degree_of_variable_string = String.Empty;
            string coefficient_string = String.Empty;
            foreach (char c in TermString)
            {
                if (c == '-' || c == '+')
                {
                    if (!sign_acceptable)
                    {
                        throw new TermException(InvalidTermNotationMessage);
                    }
                    // add sign symbole to coefficient_string
                    coefficient_string += c.ToString();
                    // adjust status flags and acceptable variables
                    sign_acceptable = false;
                }
                else if (Char.IsLetter(c))
                {
                    if (!variable_acceptable)
                    {
                        throw new TermException(InvalidTermNotationMessage);
                    }
                    // convert any upper char to lower char
                    char lower_c;
                    if (Char.IsUpper(c))
                    {
                        lower_c = Char.ToLower(c);
                    }
                    else
                    {
                        lower_c = c;
                    }
                    // if the variable exist in variables dictionary, throw exception
                    if (variables.ContainsKey(lower_c))
                    {
                        throw new TermException(InvalidTermNotationMessage);
                    }
                    // if previous loop was parsing degree of a variable, update the degree of previous variable
                    if (parsing_degree_of_variable && previous_variable != '\0')
                    {
                        parsing_degree_of_variable = false;
                        try
                        {
                            variables[previous_variable] = int.Parse(degree_of_variable_string);
                        }
                        catch (Exception e)
                        {
                            throw new TermException("Invalid degree of variable: " + e.Message);
                        }
                    }
                    // if there is no explicity coeffcient, make coefficient have default value: 1
                    if (coefficient_string == String.Empty || coefficient_string == "+" )
                    {
                        coefficient_string = "1";
                    }
                    if (coefficient_string == "-")
                    {
                        coefficient_string = "-1";
                    }
                    // if Coefficient is not parsed yet, parse Coefficient with coefficient_string
                    if (Coefficient == 0.0 && coefficient_string != String.Empty)
                    {
                        try
                        {
                            Coefficient = float.Parse(coefficient_string);
                            parsing_coefficient = false;  // set false when we parsed Coefficient
                        }
                        catch (Exception e)
                        {
                            throw new TermException("Invalid coefficient: " + e.Message);
                        }
                    }
                    // add the variable to variables dict with default coefficient:1
                    variables.Add(lower_c, 1);
                    previous_variable = lower_c;
                    // adjust status flags and acceptable variables
                    sign_acceptable = false;
                    number_acceptable = false;
                    degree_sign_acceptable = true;
                }
                else if (c == '^')
                {
                    if (!degree_sign_acceptable)
                    {
                        throw new TermException(InvalidTermNotationMessage);
                    }
                    degree_of_variable_string = String.Empty;
                    // adjust status flags and acceptable variables
                    number_acceptable = true;
                    degree_sign_acceptable = false;
                    variable_acceptable = false;
                    parsing_degree_of_variable = true;
                }
                else if (Char.IsNumber(c) || c == '.')
                {
                    // if number not acceptable or degree of variable string has .
                    if (!number_acceptable || (parsing_degree_of_variable && c == '.'))
                    {
                        throw new TermException(InvalidTermNotationMessage);
                    }
                    // append number to proper parsing string
                    if (parsing_coefficient && !parsing_degree_of_variable)
                    {
                        coefficient_string += c.ToString();
                    }
                    else if (parsing_degree_of_variable)
                    {
                        degree_of_variable_string += c.ToString();
                    }
                    // adjust status flags and acceptable variables
                    variable_acceptable = true;
                }
            }
            // if previous loop was parsing degree of a variable, update the degree of previous variable
            if (parsing_degree_of_variable && previous_variable != '\0')
            {
                try
                {
                    variables[previous_variable] = int.Parse(degree_of_variable_string);
                }
                catch (Exception e)
                {
                    throw new TermException("Invalid degree of variable: " + e.Message);
                }
            }
            // if Coefficient is not parsed yet, this means there is no variables
            if (Coefficient == 0.0 && coefficient_string != String.Empty)
            {
                try
                {
                    Coefficient = float.Parse(coefficient_string);
                    parsing_coefficient = false;  // set false when we parsed Coefficient
                }
                catch (Exception e)
                {
                    throw new TermException("Invalid coefficient: " + e.Message);
                }
            }
            // build term key with sorted variables
            Key = "";
            foreach(KeyValuePair<char, int> kvp in variables)
            {
                if (kvp.Value == 1)
                {
                    Key += String.Format("{0}", kvp.Key);
                }
                else
                {
                    Key += String.Format("{0}^{1}", kvp.Key, kvp.Value);
                }
            }
        }
    }
}
