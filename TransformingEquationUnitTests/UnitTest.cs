﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TransformingEquation.Models;
using System.Collections.Generic;

namespace TransformingEquationUnitTests
{
    [TestClass]
    public class UnitTest
    {
        private SortedDictionary<string, Term> terms = new SortedDictionary<string, Term>(new TermKeyComparer());

        [TestMethod]
        public void TestMethodForTermKeyComparer()
        {
            terms.Add("x^2y", new Term("x^2y"));
            terms.Add("xy", new Term("2xy"));
            terms.Add("xy^2", new Term("2xy^2"));
            terms.Add("x^2y^2z", new Term("x^2y^2z"));
            terms.Add("x^2yz^2", new Term("x^2yz^2"));
            terms.Add("x^3", new Term("3x^3"));
            terms.Add("x^21", new Term("4x^21"));
            terms.Add("y", new Term("5y"));
            terms.Add("x", new Term("5x"));
            terms.Add("y^2", new Term("5y^2"));
            terms.Add("", new Term("-3.5"));
            string[] expected_sort = new string[11] {
                "x^21", "x^3", "x^2y^2z", "x^2yz^2", "x^2y", "xy^2", "xy", "x", "y^2", "y", ""
            };
            int index = 0;
            foreach (KeyValuePair<string, Term> kvp in terms)
            {
                Assert.AreEqual(expected_sort[index++], kvp.Key);
            }
        }

        [TestMethod]
        public void TestMethodForEquation1()
        {
            Equation eq = new Equation("x^2 + 3.5xy + y = y^2 - xy + y");
            Assert.AreEqual(eq.Input_equation_line, "x^2 + 3.5xy + y = y^2 - xy + y");
            Assert.AreEqual(eq.ToString(), "x^2 + 4.5xy - y^2 = 0");
        }

        [TestMethod]
        public void TestMethodForEquation2()
        {
            Equation eq = new Equation("x^2 + x^2y^3 + 3.5xy + y = y^2 - xy + y");
            Assert.AreEqual(eq.Input_equation_line, "x^2 + x^2y^3 + 3.5xy + y = y^2 - xy + y");
            Assert.AreEqual(eq.ToString(), "x^2y^3 + x^2 + 4.5xy - y^2 = 0");
        }

        [TestMethod]
        public void TestMethodForEquation3()
        {
            Equation eq = new Equation("x = 1");
            Assert.AreEqual(eq.Input_equation_line, "x = 1");
            Assert.AreEqual(eq.ToString(), "x - 1 = 0");
        }

        [TestMethod]
        public void TestMethodForEquation4()
        {
            Equation eq = new Equation("x - 1 = 0");
            Assert.AreEqual(eq.Input_equation_line, "x - 1 = 0");
            Assert.AreEqual(eq.ToString(), "x - 1 = 0");
        }

        [TestMethod]
        public void TestMethodForEquation5()
        {
            Equation eq = new Equation("0 = 0");
            Assert.AreEqual(eq.Input_equation_line, "0 = 0");
            Assert.AreEqual(eq.ToString(), "0 = 0");
        }

        [TestMethod]
        public void TestMethodForEquation6()
        {
            Equation eq = new Equation("1 - 2 + 1 = 0");
            Assert.AreEqual(eq.Input_equation_line, "1 - 2 + 1 = 0");
            Assert.AreEqual(eq.ToString(), "0 = 0");
        }

        [TestMethod]
        public void TestMethodForEquation7()
        {
            Equation eq = new Equation("1 - 2 + 1 = 1 - 2 + 1");
            Assert.AreEqual(eq.Input_equation_line, "1 - 2 + 1 = 1 - 2 + 1");
            Assert.AreEqual(eq.ToString(), "0 = 0");
        }

        [TestMethod]
        public void TestMethodForEquation8()
        {
            Equation eq = new Equation("x^2 + x^2y^3 + 3.5xy + y = x^2 + x^2y^3 + 3.5xy + y");
            Assert.AreEqual(eq.Input_equation_line, "x^2 + x^2y^3 + 3.5xy + y = x^2 + x^2y^3 + 3.5xy + y");
            Assert.AreEqual(eq.ToString(), "0 = 0");
        }

        [TestMethod]
        public void TestMethodForEquation9()
        {
            Equation eq = new Equation("x - (y^2 - x) = 0");
            Assert.AreEqual(eq.Input_equation_line, "x - (y^2 - x) = 0");
            Assert.AreEqual(eq.ToString(), "2x - y^2 = 0");
        }

        [TestMethod]
        public void TestMethodForEquation10()
        {
            Equation eq = new Equation("x - ((y^2 - x) - z) - xy = 0");
            Assert.AreEqual(eq.Input_equation_line, "x - ((y^2 - x) - z) - xy = 0");
            Assert.AreEqual(eq.ToString(), "-xy + 2x - y^2 + z = 0");
        }

        [TestMethod]
        public void TestMethodForEquation11()
        {
            Equation eq = new Equation("x - (z + (y^2 - x) - z) - xy = 0");
            Assert.AreEqual(eq.Input_equation_line, "x - (z + (y^2 - x) - z) - xy = 0");
            Assert.AreEqual(eq.ToString(), "-xy + 2x - y^2 = 0");
        }

        [TestMethod]
        public void TestMethodForEquation12()
        {
            Equation eq = new Equation("x - (z + (y^2 - x) - z) - xy = x - (y^2 - x)");
            Assert.AreEqual(eq.Input_equation_line, "x - (z + (y^2 - x) - z) - xy = x - (y^2 - x)");
            Assert.AreEqual(eq.ToString(), "-xy = 0");
        }

        [TestMethod]
        public void TestMethodForEquation13()
        {
            Equation eq = new Equation("x - (0 - (0 - x)) = 0");
            Assert.AreEqual(eq.Input_equation_line, "x - (0 - (0 - x)) = 0");
            Assert.AreEqual(eq.ToString(), "0 = 0");
        }

        [TestMethod]
        public void TestMethodForTermKey()
        {
            Term x2y = new Term("x^2y");
            Assert.AreEqual(x2y.Key, "x^2y");
            Assert.AreEqual(x2y.Coefficient, 1.0);
            Assert.AreEqual(x2y.ToString(), "x^2y");
        }

        [TestMethod]
        public void TestMethodForTermWithoutVariables()
        {
            Term x2y = new Term("-1.0");
            Assert.AreEqual(x2y.Key, String.Empty);
            Assert.AreEqual(x2y.Coefficient, -1.0);
            Assert.AreEqual(x2y.ToString(), "-1");
        }

        [TestMethod]
        public void TestMethodForTermOnlyWithZeroConstant()
        {
            Term x2y = new Term("0");
            Assert.AreEqual(x2y.Key, String.Empty);
            Assert.AreEqual(x2y.Coefficient, 0.0);
            Assert.AreEqual(x2y.ToString(), "0");
        }

        [TestMethod]
        public void TestMethodForTermKeyWithMinus()
        {
            Term x2y = new Term("-x^2y");
            Assert.AreEqual(x2y.Key, "x^2y");
            Assert.AreEqual(x2y.Coefficient, -1.0);
            Assert.AreEqual(x2y.ToString(), "-x^2y");
        }

        [TestMethod]
        public void TestMethodForTermKeyWithUpperVariable()
        {
            Term x2y = new Term("X^2y");
            Assert.AreEqual(x2y.Key, "x^2y");
            Assert.AreEqual(x2y.Coefficient, 1.0);
            Assert.AreEqual(x2y.ToString(), "x^2y");
        }

        [TestMethod]
        public void TestMethodForTermKeyWithZeroCoefficient()
        {
            Term x2y = new Term("0x^2y");
            Assert.AreEqual(x2y.Key, "x^2y");
            Assert.AreEqual(x2y.Coefficient, 0.0);
            Assert.AreEqual(x2y.ToString(), String.Empty);
        }

        [TestMethod]
        public void TestMethodForTermKeyWithPlusCoefficient()
        {
            Term x2y = new Term("23.5x^2y");
            Assert.AreEqual(x2y.Key, "x^2y");
            Assert.AreEqual(x2y.Coefficient, 23.5);
            Assert.AreEqual(x2y.ToString(), "23.5x^2y");
        }

        [TestMethod]
        public void TestMethodForTermKeyWithUnorderedVariables()
        {
            Term x2y = new Term("23.5Zx^2y^3");
            Assert.AreEqual(x2y.Key, "x^2y^3z");
            Assert.AreEqual(x2y.Coefficient, 23.5);
            Assert.AreEqual(x2y.ToString(), "23.5x^2y^3z");
        }

        [TestMethod]
        public void TestMethodForTermKeyWithMinusCoefficient()
        {
            Term x2y = new Term("-23.5x^2y");
            Assert.AreEqual(x2y.Key, "x^2y");
            Assert.AreEqual(x2y.Coefficient, -23.5);
            Assert.AreEqual(x2y.ToString(), "-23.5x^2y");
        }

        [TestMethod]
        public void TestMethodForTermException()
        {
            try
            {
                Term error = new Term("error");
            }
            catch (TermException e)
            {
                StringAssert.Contains(e.Message, "Invalid Term Notation");
            }
        }

        [TestMethod]
        public void TestMethodForTermExceptionWithFloatDegreeOFVariable()
        {
            try
            {
                Term error = new Term("x^2.5y");
            }
            catch (TermException e)
            {
                StringAssert.Contains(e.Message, "Invalid Term Notation");
            }
        }

        [TestMethod]
        public void TestMethodForTermExceptionWithDuplicatedVariable()
        {
            try
            {
                Term error = new Term("x^2yx");
            }
            catch (TermException e)
            {
                StringAssert.Contains(e.Message, "Invalid Term Notation");
            }
        }

    }
}
